# What is this?

Using rsyslog with RELP and GnuTLS currently (December 2020) sporadically
results in semi-broken connections (see [these bug reports for
details](https://github.com/rsyslog/rsyslog/issues?q=2353)). Monitoring for
these failures and restarting `rsyslogd` is a quick and dirty fix.


## Building

Run a Debian 10 container (or use a Debian 10 system):

```
podman run -it --rm --security-opt label=disable -v .:/mnt debian:10
```
Install dependencies inside container and build:
```
apt update
apt upgrade -y
apt install -y gccgo golang git libsystemd-dev

cd /mnt # or wherever the source code is
go build -ldflags="-s -w"
```

## Installation

Run as `root`:

```
install --group=root --owner=root --mode=0700 restartbrokensystemdunit /usr/local/sbin
install --group=root --owner=root --mode=0644 restartbrokensystemdunit.service /etc/systemd/system
systemctl daemon-reload
systemctl enable restartbrokensystemdunit.service
systemctl start restartbrokensystemdunit.service
```

## via Ansible

see [https://git.scc.kit.edu/mail/ansible/mailinout/-/tree/master/roles/restartbrokensystemdunit](https://git.scc.kit.edu/mail/ansible/mailinout/-/tree/master/roles/restartbrokensystemdunit)
