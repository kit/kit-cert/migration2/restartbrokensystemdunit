package main

import (
	"errors"
	"log"
	"regexp"
	"time"

	"github.com/coreos/go-systemd/v22/dbus"
	"github.com/coreos/go-systemd/v22/sdjournal"
)

type RestarterConfig struct {
	SystemdUnitName string
	Pattern         []*regexp.Regexp
}

var (
	RsyslogWatcher = RestarterConfig{
		SystemdUnitName: "rsyslog.service",
		Pattern: []*regexp.Regexp{
			regexp.MustCompile(`librelp error 10007\[connection broken\] forwarding to server`),
			regexp.MustCompile(`omrelp\[.*\]: error 'error opening connection to remote peer',`),
			regexp.MustCompile(`omrelp\[.*\]: error 'io error in RawSendCommand, session broken'`),
			regexp.MustCompile(`liblogging-stdlog\[\d+\]: unexpected GnuTLS error`),
			regexp.MustCompile(`imrelp\[\d+\]: error 'TLS record reception failed \[gnutls error -54: Error in the pull function\.\]'`),
			regexp.MustCompile(`imrelp\[\d+\]: error 'error when receiving data, session broken',`),
			regexp.MustCompile(`imrelp\[\d+\]: error 'TLS record write failed \[gnutls error -10: The specified session has been invalidated for some reason\.\]',`),
		},
	}
)

type JournalRestartOnMatch struct {
	dbConn *dbus.Conn
	RestarterConfig
}

func NewJournalRestartOnMatch(dbc *dbus.Conn, conf RestarterConfig) JournalRestartOnMatch {
	return JournalRestartOnMatch{
		dbConn:          dbc,
		RestarterConfig: conf,
	}
}

func (j JournalRestartOnMatch) Write(p []byte) (n int, err error) {
	var logLine = string(p)

	for _, r := range j.Pattern {
		//log.Printf("DEBUG: %s\n", logLine)
		if r.MatchString(logLine) {
			log.Print("Match found, restarting unit")
			_, err := j.dbConn.RestartUnit(j.SystemdUnitName, "replace", nil)
			if err != nil {
				return len(p), err
			}
			return len(p), nil
		}
	}

	return len(p), nil
}

func WatchAndRestart(wc RestarterConfig) error {
	// open journal
	journalReader, err := sdjournal.NewJournalReader(sdjournal.JournalReaderConfig{
		NumFromTail: 1,
		Matches: []sdjournal.Match{
			{
				Field: sdjournal.SD_JOURNAL_FIELD_SYSTEMD_UNIT,
				Value: wc.SystemdUnitName,
			},
		},
	})

	if err != nil {
		return errors.New("Error opening journal: %s" + err.Error())
	}

	if journalReader == nil {
		return errors.New("Got a nil reader")
	}

	defer journalReader.Close()
	log.Print("Connection to system journal established.")

	// connect to systemd dbus
	dbusConn, err := dbus.NewSystemConnection()
	if err != nil {
		return errors.New("Error opening dbus connection to systemd: %s" + err.Error())
	}

	defer dbusConn.Close()
	log.Print("Connection to systemd dbus established.")

	// create io.Reader that restarts the unit on error
	logreader := NewJournalRestartOnMatch(dbusConn, RsyslogWatcher)

	// read logs forever
	quitFollowChan := make(chan time.Time)
	journalReader.Follow(quitFollowChan, logreader)

	return nil
}

func main() {
	err := WatchAndRestart(RsyslogWatcher)
	if err != nil {
		log.Fatal(err)
	}
}
