package main

import "testing"

func TestRsyslogdPattern(t *testing.T) {
	var (
		matches int

		triggerLines = []string{
			// Sender
			`omrelp[hostname:67890]: error 'error opening connection to remote peer', object  'conn to srvr hostname:port' - action may not work as intended [v8.1901.0 try https://www.rsyslog.com/e/2353 ]`,
			`omrelp[hostname:67890]: error 'io error in RawSendCommand, session broken', object  'conn to srvr hostname:port' - action may not work as intended [v8.1901.0 try https://www.rsyslog.com/e/2353 ]`,
			`librelp error 10007[connection broken] forwarding to server hostname:port - suspending  [v8.1901.0 try https://www.rsyslog.com/e/2291 ]`,

			`liblogging-stdlog[12345]: unexpected GnuTLS error -53 - this could be caused by a broken connection. GnuTLS reports: Error in the push function.   [v8.24.0 try http://www.rsyslog.com/e/2078 ]`,
			`liblogging-stdlog[12345]: unexpected GnuTLS error -54 in nsd_gtls.c:1755: Error in the pull function.  [v8.24.0 try http://www.rsyslog.com/e/2078 ]`,
			// Receiver
			`imrelp[12345]: error 'TLS record reception failed [gnutls error -54: Error in the pull function.]', object  'lstn 20514: conn to clt ip/hostname' - input may not work as intended [v8.1901.0 try https://www.rsyslog.com/e/2353 ]`,
			`imrelp[12345]: error 'error when receiving data, session broken', object  'lstn 20514: conn to clt ip/hostname' - input may not work as intended [v8.1901.0 try https://www.rsyslog.com/e/2353 ]`,
			`imrelp[12345]: error 'TLS record write failed [gnutls error -10: The specified session has been invalidated for some reason.]', object  'lstn 20514: conn to clt ip/hostname' - input may not work as intended [v8.1901.0 try https://www.rsyslog.com/e/2353 ]`,
		}
	)

	for _, line := range triggerLines {
		matches = 0
		for _, pattern := range RsyslogWatcher.Pattern {
			if pattern.MatchString(line) {
				matches++
			}
		}
		if matches == 0 {
			t.Fatalf("Log line »%s« was never matched", line)
		}
	}
}
