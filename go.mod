module git.scc.kit.edu/KIT-CERT/restartbrokensystemdunit

go 1.14

require github.com/coreos/go-systemd/v22 v22.1.0
